const mqtt = require('mqtt')
const Influx = require('influx');
const devID = 'INEM_DEMO';
const clientOption = {
  port: 1883,
  host: 'localhost',
  protocol: 'mqtt'
};
const client = mqtt.connect(clientOption)


const influx = new Influx.InfluxDB({
  host: 'localhost',
  database: 'Faclon',

});
influx.getDatabaseNames()
  .then(names => {
    if (!names.includes('Faclon')) {
      return influx.createDatabase('Faclon');
    }
  })
  .catch(err => {
    console.log(err)
  })


client.on('connect', function () {

  client.subscribe(`devicesIn/INEM_DEMO/data`, function () {

    client.on('message', function (topic, message, packet) {
      // message = JSON.parse(message)
      let msg = JSON.parse(message);
   
      console.log(JSON.parse(message));
         for(var i=0 ; i<38 ; i++){
      let data = [
        { measurement: devID, timestamp: msg['time'], fields: { sensor: msg['data'][i]['tag'], value:  msg['data'][i]['value'] } }
      ]
      influx.writePoints(data,{precison: "ms" }).catch(err => {
        console.error(`Error saving data to InfluxDB! ${err.stack}`)
      })
     }
      });
  });
});


