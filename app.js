const mqtt = require('mqtt');
const Influx = require('influx');
const express = require("express");
const XLSX = require('xlsx')
const ExcelJS = require('exceljs');
const moment = require('moment')
const devID = 'INEM_DEMO';


const {mqttClient} = require('./config/config')
const { readMessage } = require('./rec')

const app=express();
app.use(express.json())


const influx = new Influx.InfluxDB({
  host: 'localhost',
  database: 'Faclon',

});
influx.getDatabaseNames()
  .then(names => {
    if (!names.includes('Faclon')) {
      return influx.createDatabase('Faclon');
    }
  })
  .catch(err => {
    console.log(err)
  })
  const client = mqtt.connect(mqttClient)
  client.on('connect', ()=> { 
      console.log("mqtt connected")
  });
  client.subscribe(`devicesIn/INEM_DEMO/data`, readMessage(client,influx))

  app.get('/excel', async( req ,res)=>{
    try{
      let st = req.body.st;
      let et = req.body.et;
        const results = await influx.query(`select * FROM INEM_DEMO WHERE time >= ${st} AND time <= ${et}`)
console.log(st,et);
console.log(JSON.stringify(results));
        const workbook = new ExcelJS.Workbook()
        const sheet = workbook.addWorksheet("DataBetweenStandEt")
        sheet.columns = [
            {header: "time", key: "time", width:45},
            {header: "sensor", key:"sensor", width:20},
            {header: "value", key: "value", width:40},
            
        ]
        for(var j = 0;j<results.length;j++){
          results[j].time = moment(results[j].time).format('DD/MM/YYYY HH:MM:SS')
            sheet.addRow(results[j])
        }
        const output = await workbook.xlsx.writeFile('DataBetweenStandEt.XLSX');
        res.send(results);
    }
    catch(error){
       console.log(error);
    }
});
app.listen(4000,()=>{ 
  console.log("server connected")
})










